FROM node:14-alpine
ENV NODE_ENV="production"
RUN adduser -D -h /home/app app 
USER app
WORKDIR /home/app
COPY --chown=app:app . /home/app
RUN npm install /home/app
EXPOSE 3000
CMD ["node","/home/app/index.js"]